﻿#include "adv.h"
#include <stdio.h>

void adv_m() {
	adv_ptr_fun();
	return;
}

void adv_ptr_fun() {
	printf("I AM adv_ptr_fun()\n");
	int(*ptr_int)(int) = 0x0;
	adv_print(&ptr_int, "ptr_int");

	ptr_int = &adv_dummy_fun_0;


	return;
}

void adv_complex() {
	printf("I AM adv_complex()\n");
	float a = 7;
	printf("%d", *(unsigned int *)(&a) >> 23);
	return;
}


void adv_print(const void* what, const char whatname[]) {
	printf("%s %%x == %x\n", &whatname, what);
	printf("%s %%p == %p\n", &whatname, what);
	printf("%s %%d == %d\n", &whatname, what);
	printf("%s %%f == %f\n\n", &whatname, what);

	/* tricky thing follows. compiler dunno about what "what" is.
	so we cast [1] the val of ptr [2] as void (void is a placeholder) ptr [1.1] which is a fun eating int [1.2].
	1. (
		1.1. void(*)
		1.2. (int)
		)
	2. what					*/
	printf("%s %%x == %x\n", &whatname, (void(*)(int))what);
	printf("%s %%p == %p\n", &whatname, (void(*)(int))what);
	printf("%s %%d == %d\n", &whatname, (void(*)(int))what);
	printf("%s %%f == %f\n\n", &whatname, (void(*)(int))what);
	return;
}

int	adv_dummy_fun_0(int test) {
	return test^5;
}

float adv_dummy_fun_1(int test) {
	return (float)(test^5);
}

void undefined_behaviour() {
	return;
}
