TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

CONFIG += kr-tasks

base {
	TARGET = c_pg
	SOURCES += main.c \
		var.c \
		libc/io.c \
		libc/sockets.c \
		var_extern.c \
		oper.c \
		adv.c \
		error.c \
		pg.c

	HEADERS += \
		var.h \
		libc/io.h \
		libc/sockets.h \
		stdafx.h \
		var_extern.h \
		oper.h \
		preproc.h \
		adv.h \
		error.h \
		pg.h
}

kr-tasks {
	TARGET += kr-tasks
	SOURCES += kr-tasks/c1_8.c
}

QMAKE_CFLAGS += -std=c11
QMAKE_CFLAGS += -Wno-unused-value
QMAKE_CFLAGS += -Wno-unused-variable
QMAKE_CFLAGS += -Wformat

PRECOMPILED_HEADER += stdafx.h

win32 {
	DEFINES += _WIN32
	LIBS += D:\progs\qt\Tools\mingw48_32\i686-w64-mingw32\lib\libwsock32.a
}
