﻿#define ERROR_C_DISABLE

void error_m() {
	return;
}

void error_scope() {
	printf("I AM error_scope()\n");
#ifndef ERROR_C_DISABLE
	/* ERROR 1: using variables which were defined inside blocks */
	if (1) {
		short e1a = 0xffff;				// 65535
	} else {
		short e1a = 65535;				// 65535
	}
	e1a = 1;				// error: 'e1a' undeclared (first use in this function)

	{
		int e1b = 0xffffffff;
	}
	e1b = 0x0;				// error: 'e1a' undeclared (first use in this function)

	/* ERROR 2: defining same vars several times; even if they shouldn't actually be redefined */
	if (1) goto hdef;
	short e2a = 65535;
	goto hdefend;
	hdef:
	short e2a = 0xffff;
	hdefend:

	e2a = 123;
#endif // ERROR_C_DISABLE
	return;
}
