﻿#include <stdio.h>

static int experiments() {
	printf("\\a: \"\a\"\n");		/*  */
	printf("\\b: \"\b\"\n");		/* backspace */
	printf("\\c: \"\c\"\n");
	printf("\\d: \"\d\"\n");
	printf("\\e: \"\e\"\n");		/*  */
	printf("\\f: \"\f\"\n");		/*  */
	printf("\\g: \"\g\"\n");
	printf("\\h: \"\h\"\n");
	printf("\\i: \"\i\"\n");
	printf("\\j: \"\j\"\n");
	printf("\\k: \"\k\"\n");
	printf("\\l: \"\l\"\n");
	printf("\\m: \"\m\"\n");
	printf("\\n: \"\n\"\n");		/* newline */
	printf("\\o: \"\o\"\n");
	printf("\\p: \"\p\"\n");
	printf("\\q: \"\q\"\n");
	printf("\\r: \"\r\"\n");		/*  */
	printf("\\s: \"\s\"\n");
	printf("\\t: \"\t\"\n");		/* tab */
	//printf("\\u: \"\u\"\n");
	printf("\\u: \"\u0105\"\n");	/*  */
	printf("\\v: \"\v\"\n");		/*  */
	printf("\\w: \"\w\"\n");
	//printf("\\x: \"\x\"\n");
	printf("\\x: \"\xFF\"\n");		/*  */
	printf("\\y: \"\y\"\n");
	printf("\\z: \"\z\"\n");

	printf("\\\\: \"\\\"\n");		/* bslash */
	printf("\\\": \"\"\"\n");		/* 2xQ */
	printf("\\\': \"\'\"\n");		/* 1xQ */
	return 0;
}

static void eprintf() {
	for (char v = 'a'-1;
		 ++v <= 'z';
		 printf("printf(\"\\\\%c: \\\"\\%c\\\"\\n\");\n", v, v));
	return;
}

int main() {
	printf("Hello, World!\nexperiments:\n\n");
	experiments();
	return 0;
}
