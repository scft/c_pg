﻿#include <stdio.h>

int c1_5() {
	printf("\n\n");
	const int lower = 0, upper = 300, step = 20;
	for (int fahr = lower; fahr <= upper; fahr += step) printf("%3d\t %6.1f\n", fahr, (5.0*9.0)*(fahr-32));

	printf("\n\nreverse order:\n");
	for (int fahr = upper; fahr >= lower; fahr -= step) printf("%3d\t %6.1f\n", fahr, (5.0*9.0)*(fahr-32));
	return 0;
}

int main() {
	const int lower = 0, upper = 300, step = 20;
	float fahr = (float)lower, celsius;

	printf("Fahr:\t\tCelsius:\n"
		   "------------------------------\n");
	while (fahr <= upper) {
		printf("%3.0f\t\t%6.1f\n",
			   fahr,
			   celsius = (fahr-32.0)*(5.0/9.0));
		fahr += step;
	}
	fahr = 0; celsius = -17.8;

	/* TODO: is there some tricky way to make fahr real,
		except for storing upper data to array and re-using it? */
	const float cstep = 11.11111111;
	printf("\n\nCelsius:\tFahr:\n"
		   "------------------------------\n");
	while (fahr < upper-1) {
		printf("%6.1f\t\t%3.0f\n",
			   celsius,
			   fahr = celsius*(9.0/5.0)+32.0);
		celsius += cstep;
	}

	return c1_5();
}
