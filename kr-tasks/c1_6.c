﻿#include <stdio.h>
int main() {
	for (int c; (c = getchar()) != EOF; putchar(c));

	for (int i = 0; i != 10; i++ && printf("%d\n", getchar() != EOF));

	printf("%d", EOF);

	return 0;
}
