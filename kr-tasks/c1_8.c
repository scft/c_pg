﻿#include <stdio.h>

long ccnt() {
	long nc;
	for (nc = 0; getchar() != EOF; ++nc);
	return nc;
}

double ccnt2() {
	double nc;
	for (nc = 0; getchar() != EOF; ++nc);
	return nc;
}

#define EXERC18	1
int nlcnt() {
	for (int nc = 0;;) {
		switch (getchar()) {
#ifdef EXERC18
			case '\t':
			case ' ':
#endif
			case '\n':
			nc++;
			break;

			case EOF:
			return nc;
			default:
			break;
		}
	}
	return -1;
}

/* TODO: self-review */
void cpio_replace() {
	for (int c = 0, d = 0; (c = getchar()) != EOF; putchar(c)) {
		bgn:
		if (c == ' ' && d == 0) d = 1;
		else if (c != ' ') d = 0;
		else { c = getchar(); goto bgn; }
	}
	return;
}

int main() {
	printf("ccnt(): %ld\n"
		   "ccnt2(): %.0f\n"
		   "nlcnt(): %d\n"
		   "\n", ccnt(), ccnt2(), nlcnt());

	printf("cpio_replace():\n");
	cpio_replace();
	return 0;
}
