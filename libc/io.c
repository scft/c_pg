﻿/*	mostly various helloworlds here. */
#include "io.h"
#include <stdio.h>

#define OS_MINGW 1

void lc_io_m() {
	lc_io_hellouniverse();
	return;
}

void lc_io_hellouniverse() {
	printf("Hello, universe.\n");
}
