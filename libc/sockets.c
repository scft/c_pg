﻿/*	Playing with sockets
	Windows documentation:	http://msdn.microsoft.com/en-us/library/windows/desktop/ms741394(v=vs.85).aspx
	BSD libc:		http://www.freebsd.org/doc/en_US.ISO8859-1/books/developers-handbook/sockets-essential-functions.html
*/

#ifdef LIBC_BSD
#include <sys/types.h>
#include <sys/socket.h>
#elif LIBC_MINGW
#include <winsock2.h>
#elif LIBC_GNU
#endif

#include <stdio.h>

#include "sockets.h"

#define SK_PORT 7030

void sk_manage() {
//	struct sk* sk;
//	sk->port = 7025;

													// just curious if define will work here
	printf("running http server. look to 127.0.0.1:SK_PORT");
	char test[] = "SK_PORT";
	printf(test);

	if (sk_http_give() != 0) printf("http server failed");
	return;
}

void sk_read(struct sk* sk, void* buf, int buflen) {
	return;
}

void sk_send(struct sk* sk, void* buf, int buflen) {
	return;
}

int sk_http_give() {
	const char html[] = "HTTP/1.1 200 OK\r\n"
						"Connection: close\r\n"
						"Server: sockpg\r\n"
						"Content-Type: text/html\r\n"
						"\r\n"
						"<html><head><title>STAPH RIGHT HERE!</title></head>"
						"<body>"
						"<h4>You may think that i'm a fat server, yet in fact i'm not.</h1>"
						"<p><!-- TBD: show time or some other dynamic variable here -->asdfasdsdfgsdfg</p>"
						"</body></html>";

#ifdef LIBC_MINGW 1
	WSADATA mwsadata;
	SOCKET msock;
	SOCKADDR_IN msockaddr_in;
#endif

	char* p = NULL;

	struct sk sk = { NULL, 0, 2 };
	sk.port = SK_PORT;
	sk.verbose = 1;

#ifdef LIBC_MINGW 1
	/* initiate usage of winsock2
	first argument is required winsock version (i presume it is) */
	if (WSAStartup(MAKEWORD(2,2), &mwsadata) != 0) return 1;

	/* create a socket abstraction inside a lib */
	msock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (msock == INVALID_SOCKET) return 1;

	msockaddr_in.sin_port = htons(sk.port);
	msockaddr_in.sin_family = AF_INET;
	msockaddr_in.sin_addr.s_addr = htonl(INADDR_ANY);

	if (bind(msock, &msockaddr_in, sizeof msockaddr_in) == SOCKET_ERROR) return 1;

	// then goes listen, accept, clienthandle and closesocket


#endif

	return 0;
}

void inline sk_print(struct sk* sk) {
	printf("\nServed socket port: %d\n"
		   "Type: %d"
		   "Domain: %d"
		   "Protocol: %d"
		   "Verbosity: %d"
		   "\n\n",
		   sk->port, sk->type, sk->domain, sk->proto, sk->verbose);
	return;
}
