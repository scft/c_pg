﻿#ifndef SOCKETS_H
#define SOCKETS_H

struct sk {
	int port;
	int type;
	int domain;
	int proto;
	int verbose;
};

void sk_manage();
void sk_read(struct sk*, void*, int);
void sk_send(struct sk*, void* buf, int);
int sk_http_give();

#endif // SOCKETS_H
