﻿/*	A mere playground of an honest C11 learner.
	Uses every dark corner from C11 syntax, and partially libc.
	Written with hope that it'll help somebody, and me, to grasp C.

	Directory structure:
		.			basic c examples
			var.c		basic vars, ptrs, arrays and such
			struct.c	stuff individual to structs
			oper.c		operations, loops, flow control
			preproc.h	preprocessor
			adv.c		advanced stuff. callbacks and such
		kr-tasks	solutions for k&r book "the c programming language"
		cc-spec		stuff specifical to c++ language
		libc		playing with what could be named as unix api

	repo: bitbucket.org/scft/c_pg

	you are welcome to PR clarificational (or other) additions
	e.g. cleanups and new features.
	try keep the code style and general simplicity.

	license:

				DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
						Version 2, December 2004
			(c) scft 2014
		Everyone is permitted to copy and distribute verbatim or modified
		copies of this license document, and changing it is allowed as long
		as the name is changed.

				DO WHAT THE FUCK YOU WANT TO PUBLIC LICENSE
		TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION

		 0. You just DO WHAT THE FUCK YOU WANT TO.
*/


#include <stdio.h>

#include "var.h"
#include "oper.h"
#include "adv.h"
#include "pg.h"

#include "libc/io.h"
#include "libc/sockets.h"

#include "kr-tasks/chap.h"

#define MAIN_C_SWITCH

/* decorator for printf. useful when we need to use several masks at once */
int printf_dec(void* what, char* whatname[]) {
	// no need for casing - printf does everything using masks
	printf("%s %%x == %x\n", whatname, *(char*)what);	//char is just like void, yet it can be dereferenced
	printf("%s %%p == %p\n", whatname, *(char*)what);
	printf("%s %%d == %d\n", whatname, *(char*)what);
	printf("%s %%f == %f\n\n", whatname, *(char*)what);
	return 1;
}

int main() {
	printf("Playground started\n\n"
		   "where am i to go?\n");

	/* TODO: simple implementation of this switch */
	/* TODO: simple implementation of function list */

#ifdef MAIN_C_SWITCH
	switch (getchar()) {
		case 'k':
		printf("which one?\n");
		switch (getchar()) {
			case '1':
			switch (getchar()) {
				case '2':
				chap1_2();
				break;
				default:
				return 1;
				break;
			}
			break;
			default:
			return 1;
			break;
		}
		break;

		case 'v':
		var_cast();
		return 0;
		break;

		case 'b':
		var_b();
		return 0;
		break;

		case 'm':
		oper_m();
		return 0;
		break;

		case 'p':
		adv_m();
		return 0;
		break;

		case 'f':
		oper_flow();
		return 0;
		break;

		case 'a':
		pg_m();
		return 0;
		break;

		case 'z':
		wtf();
		return 0;
		break;


		default:
		return 1;
		break;

	}
#elif MAIN_C_SWBUFFER
	char fname[29];
#elif MAIN_C_PTBUFFER
#endif // MAIN_C_SWITCH


	printf("testing sockets\n");
	sk_manage();

	return 0;
}
