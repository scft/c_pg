﻿#include "oper.h"
#include <stdio.h>

#define WHATCHADOING int c,d,e,f,g,h,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z;

void oper_m() {
	oper_keyw();
	oper_arithm();
	oper_flow();
	oper_flow_advgoto();
	return;
}

void oper_keyw() {
	int a = 0x0;
	/* sizeof usually is used as if it were a function: */
	sizeof(a);
	/* yet, it's a keyword, in fact: */
	sizeof a;

	/* just like other keywords: */
	int(b);

	/* but, this won't work: */
	//static(int(c));
	//sizeof int;

	/* this - will: */
	static int(c);
	sizeof(int);

	/* conclusion: braces relate to the varname, not to the kw itself. */
	/* but, look here: */
	struct tstruct {
		int (x),(y);
	};
	typedef tstruct;
	//sizeof tstruct;				/* "expected expression before tstruct"!!1 */
	sizeof (tstruct);				/* ok */

	return;
}

void oper_arithm() {
	printf("I AM oper_arithm()\n");
	int a = 2, b = 1;
	printf("(int)a %%d == %d; (int)b %%d == %d\n",
		   (int)a, (int)b);

	/* basic stuff */
	/* everything is an expression, really. */
	;
	a;
	b;a+b;a-b;a*b;a/b;a%b;&a;&b;(int)(int)(unsigned char*)a;
	int i = 10;
	//TODO: redeclarations
	WHATCHADOING'?';

	/* "while" actually is a syntactic sugar for the particular case of for. */
	for (;;) {
		while ((i += 1) < 10);
		break; }

	/* since a and b are ints, they are casted (treated as) ints if cast is implicit. */
	printf("(int)a+(int)b %%d == %d\n", a+b);

	/* variables in actions are recursive expressions. e.g. consider this: */
	printf("a is equal to b? %d.\n"
		   "a is n.e. to b? %d.\n\n"
		   "What exists - exists, what is not exists - is not exists. Is that true? %p",
		   a == b, a != b, 0x0);

	return; yup:;;i;;w;i;1;1;;k;i;1;1;;y;0;'n';r;;f;4;m;i;l;y;;t;0;n;i;g;h;t;;w;i;t;h;;m;y;;NULL;NULL;NULL;NULL;NULL;'!';							goto yup;
}

void oper_flow() {
	printf("I AM oper_flow()\n");
	if (0)
		printf("if prints if expr == 0\n");
	else if (1)
		printf("if executes if expr == 1\n");
	else
		printf("wat");
	return;

	int i = 1;
	while (0x1) {
		if (i==5) break; i++;
	}
}

/* advanced usage of goto.
	don't make habit of it. most non-structural code:
		- leads to so called spaghetti code. twisted and tangled.

		- nonrepresentative.
		  function represens an action. object generally represents a couple of primitives
		  to which actions are applied. flow represents nothing but what to do and when.
		  which you may find sort of illogical, even though basic and primitive.
			- this says no to modular approach and ease of newbie-committance.
			  just look at notepad++. you can't just rewrite some function or object...
			  because who knows what does what and what if we delete that or that.

		- thus, hard to understand
		  since it representats nothing but flow and near-zero abstraction
			- ... even your self will look at it as
			  on your deeds in case you were drunk last night.
			- this makes it quite hard to debug and review.

		- still, i believe there are a plenty of places where goto is a decent thing.
			- eesoteric programming, for instance.

	you've been warned.		*/
void oper_flow_advgoto() {
	printf("I AM oper_flow_advgoto()\n");
	return;
}
