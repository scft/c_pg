﻿#include <stdio.h>

#include "pg.h"

struct testing {
	int nul;
	void* addr;
	int nulo;
} t1;

void pg_m() {

	struct testing test;
	test.addr = 0x12;
	t1.addr = 0x13;
	printf("%p\n%p\n%p\n%p\n%p\n",
		   &(test).addr, &((test).addr), &((t1).addr), &(t1).addr, &test);
	t1.nulo = 34535; // 1. (t1) 2. (t1.nul1) 3. *
//	printf("dddl %d\n", *(int)(t1+0x4*0x3));

	int arr[30] = { 274,274,274,274,274,274,274,274,274,274,
					274,274,274,274,274,274,274,274,274,274,
					274,274,274,274,274,274,274,274,274,274 };

	for (int i = 0; ; i++) {
		if (arr[i] != 274) {
			printf("%d\n", i);
			break;
		}
	}

	/* num */
	const long long f1 = 600851475143;		/*6857*/
	long long f2 = f1;
#ifndef ALGO2
	long long fac = 2;
#else
	long long fac = 2;
#endif
	printf("%I64d, %I64d\n", f1, f2);
	/* factorization: */
	for (; f2>1;) {
#ifndef ALGO2
		for (; f2 % fac == 0;) {
			printf("%I64d\n", fac);
			f2 /= fac;
		}
		fac += 1;
#else
		for (; f2 % fac == 0;) {
			printf("%I64d\n", fac);
			f2 /= fac;
		}
		fac += 1;
		if (fac*fac > f2) {
			if (f2 > 1) {
				printf("%I64d\n", fac); break; }
		}
#endif
	}
	printf("cprime 10001: %lu\n\n", cprime(10001, 0));		/*104743*/
	printf("coneseq: %d\n", coneseq());						/*40824*/
	printf("triplets: %d\n", ptriplets(1000));				/*???*/
	cprime_msum();						/*???*/

	return; }

/* DONE t. 7 */
#define CPRIME_LIMIT 65535
unsigned long cprime(const int num, const int prop_report) {
	if (num<3 || num>CPRIME_LIMIT) return 0;
	unsigned long base[CPRIME_LIMIT] = { 2, 3 };
	int bcond = 0;

	for (int i = 1; i+1 != num; i++) {					/* base[Icur+1] = ... */
		for (unsigned long b = base[i]+1;; b++) {		/* ... = Icur+1+? */
			for (int c = 0; b % base[c] != 0; c++) {	/* ? = ((?+1+cur) % base[ALL]) != 0 */
				 if (c == i) {							/* then we've got a prime. */
					 base[i+1] = b;
					 if (prop_report) printf("%d. %lu\n", i+2, b);
					 bcond = 1;
					 break;
				 }
			}
			if (bcond) { bcond = 0; break; }
		}
	}
	return base[num-1];
}

/* DONE t. 8 */
int coneseq() {
	char num[] = "73167176531330624919225119674426574742355349194934"
				 "96983520312774506326239578318016984801869478851843"
				 "85861560789112949495459501737958331952853208805511"
				 "12540698747158523863050715693290963295227443043557"
				 "66896648950445244523161731856403098711121722383113"
				 "62229893423380308135336276614282806444486645238749"
				 "30358907296290491560440772390713810515859307960866"
				 "70172427121883998797908792274921901699720888093776"
				 "65727333001053367881220235421809751254540594752243"
				 "52584907711670556013604839586446706324415722155397"
				 "53697817977846174064955149290862569321978468622482"
				 "83972241375657056057490261407972968652414535100474"
				 "82166370484403199890008895243450658541227588666881"
				 "16427171479924442928230863465674813919123162824586"
				 "17866458359124566529476545682848912883142607690042"
				 "24219022671055626321111109370544217506941658960408"
				 "07198403850962455444362981230987879927244284909188"
				 "84580156166097919133875499200524063689912560717606"
				 "05886116467109405077541002256983155200055935729725"
				 "71636269561882670428252483600823257530420752963450";
	int sum[5] = {};
	int best = -1, tmp;
	for (int offs = 0; num[offs+4] != '\0'; offs++) {
		for (int i = 0; i<5; i++) {
			sum[i] = (int)num[offs+i] - (int)'0';
		}
		if ((tmp = sum[0]*sum[1]*sum[2]*sum[3]*sum[4]) > best) best = tmp;
		/* TODO: array shifting */
	}
	return best;
}

/* NOT DONE t. 9 */
int* ptriplets(int lkup) {
	return 0x0;
}

/* NOT DONE t. 10 */
/* (i've made this really complicated) */
#define CPRIME_NLIMIT 21474836
void cprime_cb(const int num, int (*cb_ntaker)(unsigned long, int, int)) {
	if (num<3 || num>CPRIME_NLIMIT) return 0;
	unsigned long base[CPRIME_NLIMIT] = { 2, 3 };
	int bcond = 0;
	(*cb_ntaker)(2,0,0);(*cb_ntaker)(3,1,0);

	for (int i = 1; i+1 != num; i++) {
		for (unsigned long b = base[i]+1;; b++) {
			for (int c = 0; b % base[c] != 0; c++) {
				if (c == i) {
					base[i+1] = b;
					(*cb_ntaker)(b,i+1,0);
					bcond = 1;
					break;
				}
			}
			if (bcond) { bcond = 0; break; }
		}
	}
	return;
}

int cb_ntaker(unsigned long num, int offs, int status) {
	static unsigned long base[CPRIME_NLIMIT];
	int max = 0;

	switch (status) {
		case 0:
		base[offs] = num;
		return 0;
		break;

		case 1:
		for (int i = 0; i<=offs; i++)
			printf("base[%d] == %lu\n", i, base[i]);
		return 0;
		break;

		case 2:
		for (int i = 0; i<=CPRIME_NLIMIT-1; i++)
			if (base[i] == '\0') return i;
		return -1;
		break;

		case 3:
		for (int i = 0; i<=offs; max += base[i] && i++);
		return max;
		break;

		case 9:
		return &base;
		break;

		default:
		return -1;
		break;
	}
}

void cprime_msum() {
	cprime_cb(100000, cb_ntaker);
	cb_ntaker(0,100000,1);
	return;
}


/* pointers block */
int* test(int x, long y) {
	printf("x == %d; y == %l", x, y);

	return 0x0;
}
