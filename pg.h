﻿#ifndef PG_H
#define PG_H
void pg_m();
unsigned long cprime(const int, const int);
int coneseq();
int* ptriplets();
void cprime_cb(const int, int(*)(unsigned long, int, int));
static int cb_ntaker(unsigned long, int, int);
void cprime_msum();
int* test(int, long);
#endif // PG_H
