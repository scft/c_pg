﻿#include "var.h"
#include "var_extern.h"
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>

void var_m() {
	var_b();
	var_ptr();
	var_ptr_voidderef();
	var_arr();
	var_arr_ptr();
	var_cast();
	return;
}

struct teststru {
	/* this structure of vars will be used later.
		the compiler needs to keep in mind that its definition exists for further usage.
		this could be achieved by declaration. definition is also a declaration -
		yet it's much more convinient to use _just_ declarations for making compiler think
		that this is that, and to link the mechanism of that to the shell (declaration) of that */

	/* this, hovever, is not a definition. just a declaration.
		therefore, no memory is allocated for that yet. and it will not be alloc-d at a run-time,
		only at a compile-time. (i.e. on developer's side) */
	int x;
	int y;
};

void var_b() {
	printf("I AM var_b()\n"
		   "Side note: there are many complicated declarations here.\n"
		   "this could help you to understand them: http://cdecl.org\n"
		   "source code: http://cdecl.org/files/cdecl-blocks-2.5.tar.gz\n\n");

	printf("1. defining & declaring variables...\n");
	char b = 'f';
	/* we can't actually re-use variables inside blocks.
		this is a hack. that stuff has the same scope,
		and probably there is no stack POPs involved */
/*	const int dec = 0;
	if (dec == 1) {
		short si = 65535;					// 0x ffff
		signed int i = 2147483647;			// 0x ffff ffff
		unsigned int ui = 4294967295;		// 0x ffff ffff
		long li = 2147483647;				// 0x ffff ffff
		double di = 9223372036854775807;	// 0x 7fff ffff ffff ffff
		float fi = 4294967295;
	} else { */
		printf("\t1.0. trying out hex definitions... ");
		short si = 0xffff;				// 65535
		/* all variables are signed by default */
		signed int i = 0xffffffff;			// 2147483647
		unsigned int ui = 0xffffffff;		// 4294967295
		long li = 0xffffffff;				// 2147483647
		double di = 0x7fffffffffffffff;		// 9223372036854775807
		float fi = 0xffffffffffffffff;
		printf("done.\n");
	/*}*/

	char str[] = "string";
	auto char thesameas_str[] = "string";		/* all strings with unspecified alloc mode are auto by default. */

	printf("\t1.1. trying out const, inline and static vars... ");
	const int cantchangeme = 123;				/* will be allocated as an address, but cannot be changed */
	inline int iamnowhere = 516;				/* won't be allocated as an address, const too */
	static int stvar = 7675;					/* do not delete after return; place in memory instead of stack;*/
													/* ...and use old memory when calling again */
												/* static ints should have an initializer, else compiler may do whatever */

	printf("done.\n");

	printf("\t\t1.1.1. calling a function which increments its static var... \n");
	for (; printf("\t\t\t") && var_static() != 10;);
	printf("\t\tdone.\n");

	printf("\t\t1.1.2. trying out extern int... ");
	extern int exvar;							/* can be used externally, outside var.c */
	printf("\t\tdone.\n");

	printf("\t1.2. trying out typedef + malloc + cdecl prioritiy emphasises... ");
	typedef short uint16;
	uint16 siuint = 123;						/* typedef is also appliable to structs */
	printf("\tdone.\n");


	printf("\t1.3. trying out cdecl priority emphasise + dynamic memory alloc... ");
	typedef teststru;							/* itself becomes a typename. */
	teststru* (test) = malloc(sizeof(teststru));	/* "teststru*(test)" is precisely identical to "teststru* test." */
	free(test);
	printf("\tdone.\n");

	printf("\t1.4. trying out volatile and register kw");
	volatile int vol = 4;						/* compiler will not 1. optimize it; 2. place it in the register. only one memory! */
	const volatile int cvint = 5;				/* consts are volatile, but kwords aren't mutually exclusive */
	register int reg = 6;						/* contrary to vol, this is always located in the register. */
	//int* preg = &reg;							/* error */
	//register int* ppreg = &reg;					/* error */
	printf("\tdone.\n");

	printf("done.\n\n");

	printf("2. trying out expressions... ");
	b;si;ui;i;li;di;fi;str;						/* everything is an expression */
	cantchangeme;iamnowhere;
	exvar;stvar;siuint;
	printf;var_m;								/* even a function */
	(int)(4*23^3+4%3<<2)+(int)b;						/* useless computation, just like your life is. */
	printf("done.\n\n");

	printf("3. trying out some special aliases... \n");
	printf("\tNULL is equal to %x in HEX and to %p in PTR.\n", NULL, NULL);
	NULL == 0x0?printf("\tNULL is equal to 0x00000000\n"):0x0;
	if (true == 1 == 0x1 && false == 0 == 0x0) {
		printf("\ttrue is equal to %%d %d and %%x %x\n"
			   "\tfalse is equal to %%d %d and %%x %x\n",
			true, true, false, false);
	}
	printf("done.\n\n");

	printf("4. how do we cast from void ptr to printf?\n"
		   "well, this is easy! look:\n");
	printf("\toriginal char b == \"%c\"\n"
		   "\tprintf_dec says that:\n", b);
	printf_dec(&b, "\t\tchar b");
	printf("done.\n\n");

	printf("5. functions are variables, an actual addresses. look:\n");
	printf_dec(&var_ptr, "\tvar_ptr");			/* NB: it will print var_ptr, not &var_ptr */
	printf("done.\n\n");

	return;
}

void var_ptr() {
	var_ptr_voidderef();
	return;
}





// <so-cb-demo>
#define CB_TYPE_CHAR     0
#define CB_TYPE_FLOAT    1

struct CBUFF
{
	short total;       /* Total number of array elements */
	short size;        /* Size of each array element */
	short type;        /* Array element type */
	short used;        /* Number of array elements in use */
	short start;       /* Array index of first unread element */
	void *elements;     /* Pointer to array of elements */
};

void cbRead(struct CBUFF *buffer, void *element)
{
	if (buffer->type == CB_TYPE_CHAR)
	{
	/* The RHS of this statement is the problem */
//		*(char*)element = *(buffer->elements[buffer->start]);
	}

	/* Other cases will go here */

	buffer->start = (buffer->start + 1) % buffer->total;

	--buffer->used;
}

// </so-cb-demo>





void var_ptr_voidderef() {
	printf("I AM var_ptr_voidderef()\n");
	printf("1. dereferencing variables... \n");
	unsigned int x = 12353568;
	void* y = 0x0;
	y = &x;

	printf("\tx == &ud\n", (unsigned int)y);
	printf("done.\n\n");
	printf("2. dereferencing functions... \n");

	printf("done.\n\n");


	return;
}

void var_arr() {
	printf("I AM var_arr()\n");
	char ar1[] = "asdf";
//	char ar2[] = (char*)ar1;					 /* char ar2[] = (void*)ar1; err. */
	char* par1 = ar1;
	printf("par1 %%c == %c\n", *par1);
	printf("par1+1 %%c == %c\n", *(par1+1));


	/* adv init techniques */
	int ar2[1024] = { [0 ... 500] = 0 };
	//ar2 = { [0 ... 500] = 0 };				/* error */

	int ar3[1024] = { 2 };						/* if an array is partially initialized then all other elements go 0 */
	int ar4[2048] = {};							/* initialized to 0, too */
	return;
}

void var_arr_ptr() {
	printf("I AM var_arr_ptr()\n");
	return;
}

void var_str() {
	printf("I AM var_str()\n");
	printf("1. using struct decl/define... ");
	struct stname {
		int a, b, c;
		//int d = 0;				/* error */
	};
	struct stname a;
	typedef struct stname whatever;
	whatever icanbedefinedwoutstruct;

	struct {
		int qwer, qwerr;
		//int canibedefined = 1;	/* error. it declares and defines an instance (not a type of struct), */
									/* yet still everything inside can't be initialized */
		char canibedefined;
	} b = { 0, 532, '1' };
//	b = { 0, 532, '1' };			/* initializers however are restricted to definitions */
	whatever c = {0,1,2};

	struct emptystruct {};
	struct emptystruct test;

	printf("done.\n");

	printf("2. checking ptr stuff... \n");
	if (&c == &(c.a)) printf("\tstruct points out to itself which is a first element\n");
	c.c = 1235;
	if (*(int*)(&c+(sizeof(int))*2) == c.c) printf("\tstruct is like an array - it just can have different vars\n");
	int test1 = *(int*)(&c+1);
	int test2 = c.c;
	printf("1. %p 2. %p 3. %p 4. %p 5. %p\n", &c, &c.c, (&c+(sizeof(int)*2)), sizeof(int)*2, &c+0x8);



	//c[0] = 0;						/* this will cause an error, however, since size of an element is unfixed */
	printf("done.\n");

	return;
}

void wtf() {
	struct {
		int a,b,c;
	} c = {0,1,2};
	c.c = 1235;
	assert(0x8+0x8 == 0x10);

	/* isn't true */
	if (*(int*)(&c+(sizeof(int))*2) == c.c) printf("\t\"struct is like an array - it just can have differently sized vars\"\n");

	/* let's see why */
	printf("1. %p 2. %p 3. %p 4. %p 5. %p\n", &c, &c.c, (&c+(sizeof(int)*2)), sizeof(int)*2, (void*)(&c)+0x8);

}

void var_cast() {
	printf("I AM var_cast()\n");
	int i = 5;
	long li = (long)i;

	printf("%c\n", (char)(i+60));				/* print "A" + newline */

	li = &i;
	printf_dec(&li, "long li");

	long hexadec = 0x7b;

	/* basically, "hexadec" is an implicit expression of "(long)hexadec", provided it
		belongs to "long" type.
		If latter does somehow gets to execution, then we've nothing to lose whatsoever...	*/
	(long)hexadec == hexadec?printf("(long)hexadec == hexadec\n"):42/0;

	printf("(long)hexadec %%x == %x\n", (long)hexadec);
	printf("(long)hexadec %%l == %x\n", (long)hexadec);
	hexadec += 0x7b00;
	printf("(long)hexadec+0x7b00 %%x == %x\n", (long)hexadec);
	printf("(long)hexadec+0x7b00 %%l == %x\n", (long)hexadec);
	return;
}

int var_static() {
	static int sti = 0;
	sti += 1;

	printf("My name is var_static(). I was called %d times\n", sti);
	return sti;
}
